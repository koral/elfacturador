;;; elfacturador.el --- Interactively compile invoices

;;; Commentary:

(require 'widget)
(require 'cl)
(require 'elfacturador-db)

;;; Code:

(eval-when-compile
  (require 'wid-edit))

(defconst elfacturador-document-types '(("Fattura" . "TD01")
					("Accont/anticipo su fattura" . "TD02")
					("Accont/anticipo su parcella" . "TD03")
					("Nota Di Credito" . "TD04")
					("Nota Di Debito" . "TD05")
					("Parcella" . "TD06")
					("Autofattura" . "TD20"))
  "List of possible document type.")

(defconst elfacturador-reg-fisc '(("Ordinario" . "RF01")
				  ("Contribuenti minimi (art.1, c.96-117, L. 244/07)" . "RF02")
				  ("Agricoltura e attività connesse e pesca (artt.34 e 34-bis, DPR 633/72)" . "RF04")
				  ("Vendita sali e tabacchi (art.74, c.1, DPR. 633/72)" . "RF05")
				  ("Commercio fiammiferi (art.74, c.1, DPR  633/72)" . "RF06")
				  ("Editoria (art.74, c.1, DPR  633/72)" . "RF07")
				  ("Gestione servizi telefonia pubblica (art.74, c.1, DPR 633/72)" . "RF08")
				  ("Rivendita documenti di trasporto pubblico e di sosta (art.74, c.1, DPR  633/72)" . "RF09")
				  ("Intrattenimenti, giochi e altre attività di cui alla tariffa allegata al DPR 640/72 (art.74, c.6, DPR 633/72)" . "RF10")
				  ("Agenzie viaggi e turismo (art.74-ter, DPR 633/72)" . "RF11")
				  ("Agriturismo (art.5, c.2, L. 413/91)" . "RF12")
				  ("Vendite a domicilio (art.25-bis, c.6, DPR  600/73)" . "RF13")
				  ("Rivendita beni usati, oggetti d’arte, d’antiquariato o da collezione (art.36, DL 41/95)" . "RF14")
				  ("Agenzie di vendite all’asta di oggetti d’arte, antiquariato o da collezione (art.40-bis, DL 41/95)" . "RF15")
				  ("IVA per cassa P.A. (art.6, c.5, DPR 633/72)" . "RF16")
				  ("IVA per cassa (art. 32-bis, DL 83/2012)" . "RF17")
				  ("Altro" . "RF18")
				  ("Regime forfettario (art.1, c.54-89, L. 190/2014)" . "RF19"))
  "List of possible regimi fiscali.")

(defconst elfacturador-item-naturas '(("Escluse Ex Art. 15" . "N1")
				      ("Non Soggette" . "N2")
				      ("Non Imponibili" . "N3")
				      ("Esenti" . "N4")
				      ("Regime Del Margine / Iva Non Esposta In Fattura" . "N5")
				      ("Inversione Contabile (Per Le Operazioni In Reverse Charge Ovvero Nei Casi Di Autofatturazione Per Acquisti Extra Ue Di Servizi Ovvero Per Importazioni Di Beni Nei Soli Casi Previsti)" . "N6")
				      ("IVA Assolta In Altro Stato Ue (Vendite A Distanza Ex Art. 40 C. 3 E 4 E Art. 41 C. 1 Lett. B,  Dl 331/93; Prestazione Di Servizi Di Telecomunicazioni, Tele-Radiodiffusione Ed Elettronici Ex Art. 7-Sexies Lett. F, G, Art. 74-Sexies Dpr 633/72)" . "N7"))
  "List Of Possible Item Nature.")

(defconst elfacturador-arch-fatt "~/elfacturador/elfacturador-arch-fatt.el"
  "Name of the file where the last doc number is saved.")

(defvar elfacturador-doc-type-widget nil
  "Widget containing the selected document type.")
(defvar elfacturador-doc-number-widget nil
  "Widget containing document number.")
(defvar elfacturador-doc-month nil
  "Variable containing the document month.")
(defvar elfacturador-customer-name-widget nil
  "Widget containing the selected customer.")
(defvar elfacturador-customer-addr-widget nil
  "Widget containing the address of the customer.")
(defvar elfacturador-customer-civ-widget nil
  "Widget containing the civic number of the selected customer.")
(defvar elfacturador-customer-cap-widget nil
  "Widget containing the postal code of the selected customer.")
(defvar elfacturador-customer-com-widget nil
  "Widget containing the city of the selected customer.")
(defvar elfacturador-customer-prov-widget nil
  "Widget containing the province of the selected customer.")
(defvar elfacturador-customer-naz-widget nil
  "Widget containing the nation code of the selected customer.")
(defvar elfacturador-customer-pIVA-widget nil
  "Widget containing the IVA id of the customer.")
(defvar elfacturador-customer-pec-widget nil
  "Widget containing the PEC address of the customer.")
(defvar elfacturador-customer-sdiCode-widget nil
  "Widget containing the SDI code of the selected customer.")
(defvar elfacturador-customer-esIVA-widget nil
  "Widget containing the IVA exoneration field, only if existing.")
(defvar elfacturador-item-widgets nil
  "List of widgets used for every 'Articolo'...
second element of the list is the choice widget .")
(defvar elfacturador-button-widgets nil
  "List containing all the button's widgets except the items related ones.")
(defvar elfacturador-tot-lines nil
  "Number of the related item line.")
(defvar elfacturador-doc-number 0
  "Document number.")
(defvar elfacturador-used-iva-widgets nil
  "List of the used iva widgets.
In a form like:
\((5 ...) (10 ...)).")
(defvar elfacturador-tot-no-tax-widget nil
  "Widgets containing totals without taxes.")
(defvar elfacturador-tot-tax-widget nil
  "Widgets containing total taxes.")
(defvar elfacturador-total-widget nil
  "Widgets containing finale total.")

(defun elfacturador-completing-read (msg candidates &optional buff-name)
  "Complete user input between CANDIDATES using helm if available.
MSG is a string to prompt with.
BUFF-NAME is the buffer name created in case helm is used."
  (if (fboundp 'helm)
      (helm :sources (helm-build-sync-source msg
		       :candidates candidates)
	    :buffer buff-name)
    (completing-read msg candidates)))

(defun elfacturador-get-widget-start (widget)
  "Return the start character of a WIDGET."
  (cond ((equal 'editable-field (car widget))
	 (overlay-start (widget-get widget :field-overlay)))
	((equal 'push-button (car widget))
	 (overlay-start (widget-get widget :button-overlay)))))

(defun elfacturador-get-widget-end (widget)
  "Return the end character of a WIDGET."
  (cond ((equal 'editable-field (car widget))
	 (overlay-end (widget-get widget :field-overlay)))
	((equal 'push-button (car widget))
	 (overlay-end (widget-get widget :button-overlay)))))

(defun elfacturador-jump-to-widget (widget pos)
  "Jump directly to the desired WIDGET.
POS can be start or end"
  (let ((destination (cond ((eq 'start pos)
			    (elfacturador-get-widget-start widget))
			   ((eq 'end pos)
			    (elfacturador-get-widget-end widget))
			   ((eq 'next pos)
			    (elfacturador-get-widget-next widget)))))
    (goto-char destination)))

(defun elfacturador-format-title ()
  "Select color and title face."
  (let ((start (point-min))
	(end (- (point) 2)))
    (add-face-text-property start end 'info-title-2)
    (add-face-text-property start end '(:foreground "green"))))

(defun elfacturador-select-doc-type (&rest ignore)
  "Select a document type from db."
  (save-excursion
    (let ((doc-type (elfacturador-completing-read "Seleziona tipologia documento: "
						  elfacturador-document-types
						  "*ElFacturador Selezione Documento*")))
      (widget-value-set elfacturador-doc-type-widget doc-type))))

(defun elfacturador-update-doc-number ()
  "Generate a new doc number."
  (unless (file-exists-p elfacturador-arch-fatt)
    (elfacturador-save-doc-number))
  (load elfacturador-arch-fatt)
  (incf elfacturador-doc-number))

(defun elfacturador-save-doc-number (&rest ignore)
  "Save last document number in file ~/elfacturador/arch-fatt.el."
  (let ((number (if elfacturador-doc-number-widget
		    (string-to-number (widget-value elfacturador-doc-number-widget))
		  elfacturador-doc-number)))
    (with-temp-buffer
      (print `(setq elfacturador-doc-number ,number) (current-buffer))
      (write-file elfacturador-arch-fatt))))

(defun elfacturador-customer-widgets-create ()
  "Create widgets regarding the customer."
  (push (widget-create 'push-button
		       :notify (lambda (&rest ignore)
				 (when (equal (buffer-name) "*ElFacturador*")
				   (elfacturador-init-front-end))
				 (elfacturador-select-customer))
		       "Cliente")
	elfacturador-button-widgets)
  (setq elfacturador-customer-name-widget (widget-create 'editable-field
							 :size 57
							 :format ": %v"))
  (setq elfacturador-customer-esIVA-widget (widget-create 'editable-field
 							  :size 7
 							  :format "  Es.IVA: %v"))
  (setq elfacturador-customer-esIVA-letter-widget (widget-create 'editable-field
 								 :size 17
 								 :format "     Lettera: %v"))
  (setq elfacturador-customer-addr-widget (widget-create 'editable-field
							 :size 25
							 :format "\nIndirizzo: %v  "))
  (setq elfacturador-customer-civ-widget (widget-create 'editable-field
							:size 11
							:format "Num.:  %v  "))
  (setq elfacturador-customer-cap-widget (widget-create 'editable-field
							:size 5
							:format "CAP: %v  "))
  (setq elfacturador-customer-com-widget (widget-create 'editable-field
							:size 20
							:format "Comune: %v  "))
  (setq elfacturador-customer-prov-widget (widget-create 'editable-field
							 :size 2
							 :format "Prov.:   %v\n"))
  (setq elfacturador-customer-naz-widget (widget-create 'editable-field
							:size 2
							:format "Nazione:   %v                         "))
  (setq elfacturador-customer-pIVA-widget (widget-create 'editable-field
							 :size 11
							 :format "P.IVA: %v  "))
  (setq elfacturador-customer-pec-widget (widget-create 'editable-field
							:size 35
							:format "PEC: %v  "))
  (setq elfacturador-customer-sdiCode-widget (widget-create 'editable-field
							    :size 7
							    :format "Cod.SDI: %v"))
  (widget-insert "\n\n\n"))

(defun elfacturador-select-customer (&rest ignore)
  "Select a customer from db."
  (save-excursion
    (let* ((customer-id (elfacturador-completing-read "Seleziona un cliente: "
						     (mapcar #'car elfacturador-customers)
						     "*ElFacturador Selezione Cliente*"))
	  (customer-data (cdr (assoc customer-id elfacturador-customers))))
      (widget-value-set elfacturador-customer-name-widget (elfacturador-customer-name customer-data))
      (widget-value-set elfacturador-customer-addr-widget (elfacturador-customer-addr customer-data))
      (widget-value-set elfacturador-customer-civ-widget (elfacturador-customer-civ customer-data))
      (widget-value-set elfacturador-customer-cap-widget (elfacturador-customer-cap customer-data))
      (widget-value-set elfacturador-customer-com-widget (elfacturador-customer-com customer-data))
      (widget-value-set elfacturador-customer-prov-widget (elfacturador-customer-prov customer-data))
      (widget-value-set elfacturador-customer-naz-widget (elfacturador-customer-naz customer-data))
      (widget-value-set elfacturador-customer-pIVA-widget (elfacturador-customer-pIVA customer-data))
      (widget-value-set elfacturador-customer-pec-widget (elfacturador-customer-pec customer-data))
      (widget-value-set elfacturador-customer-sdiCode-widget (elfacturador-customer-sdiCode customer-data))
      (if (equal (elfacturador-customer-esIVA customer-data) "esente")
	  (progn (widget-value-set elfacturador-customer-esIVA-widget (elfacturador-customer-esIVA customer-data))
		 (widget-value-set elfacturador-customer-esIVA-letter-widget (elfacturador-customer-esIVA-letter customer-data)))
	(when (equal (buffer-name) "*ElFacturador*")
	  (widget-delete elfacturador-customer-esIVA-widget)
	  (widget-delete elfacturador-customer-esIVA-letter-widget)))
      (widget-setup)))
  (elfacturador-jump-to-widget (second (car elfacturador-item-widgets)) 'start))

(defun elfacturador-item-widgets-create ()
  "Create widgets for a single item."
  (incf elfacturador-tot-lines)
  (list
   (widget-create 'editable-field
		  :size 2
                  :format "Linea n°: %v   "(number-to-string elfacturador-tot-lines))
   (widget-create 'push-button
		  :notify #'elfacturador-select-item
		  "Articolo")
   (widget-create 'editable-field
                  :size 59
                  :format ": %v                   ")
   (widget-create 'push-button
		  :notify #'elfacturador-select-item-natura
		  "Natura")
   (widget-create 'editable-field
		  :size 2
		  :format ": %v\n")
   (widget-create 'editable-field
                  :size 8
                  :format "Ddt n°:   %v   ")
   (widget-create 'editable-field
                  :size 11
                  :format "Data: %v   "
		  :action (lambda (&rest ignore)
			    (widget-forward 3)))
   (widget-create 'editable-field
                  :size 9
                  :format "Prezzo: %v   ")
   (widget-create 'editable-field
                  :size 2
                  :format "U.M.: %v   ")
   (widget-create 'editable-field
                  :size 9
                  :format "Qty: %v   "
		  :action (lambda (&rest ignore)
			    (elfacturador-jump-to-widget (nth 2 elfacturador-button-widgets) 'start)))
   (widget-create 'editable-field
                  :size 10
                  :format "Totale: %v   ")
   (widget-create 'editable-field
                  :size 2
                  :format "IVA: %v")
   (widget-create 'item
                  :value "\n")))

(defun elfacturador-item-widget-line (x)
  (nth 0 x))
(defun elfacturador-item-widget-item (x)
  (nth 2 x))
(defun elfacturador-item-widget-natura (x)
  (nth 4 x))
(defun elfacturador-item-widget-ddt (x)
  (nth 5 x))
(defun elfacturador-item-widget-date (x)
  (nth 6 x))
(defun elfacturador-item-widget-price (x)
  (nth 7 x))
(defun elfacturador-item-widget-mu (x)
  (nth 8 x))
(defun elfacturador-item-widget-qty (x)
  (nth 9 x))
(defun elfacturador-item-widget-tot (x)
  (nth 10 x))
(defun elfacturador-item-widget-iva (x)
  (nth 11 x))

(defun elfacturador-item-widget-remove ()
  "Remove widgets ofthe last item line."
  (mapc (lambda (x)
   	  (widget-delete x))
   	(car elfacturador-item-widgets))
  (setq elfacturador-item-widgets (remove (car elfacturador-item-widgets) elfacturador-item-widgets))
  (decf elfacturador-tot-lines))

(defun elfacturador-select-item (widget &rest ignore)
  "Select an item from db."
  (save-excursion
    (let* ((item (elfacturador-completing-read "Seleziona un articolo: "
					       (mapcar #'car elfacturador-items)
					       "*ElFacturador Selezione Articolo*"))
	   (item-data (cdr (assoc item elfacturador-items)))
	   (widget-list (find widget elfacturador-item-widgets :test (lambda (w l)
 								       (member w l)))))
      (widget-value-set (elfacturador-item-widget-item widget-list) item)
      (widget-value-set (elfacturador-item-widget-price widget-list)(elfacturador-item-price item-data))
      (widget-value-set (elfacturador-item-widget-mu widget-list)(elfacturador-item-mu item-data))
      (widget-value-set (elfacturador-item-widget-iva widget-list)(elfacturador-item-iva item-data))))
  (widget-forward 4))

(defun elfacturador-select-item-natura (widget &rest ignore)
  "Select a particular natura for the item."
  (save-excursion
    (let* ((natura (elfacturador-completing-read "Seleziona natura articolo: "
						 elfacturador-item-naturas
						 "*ElFacturador Selezione Natura*"))
  	   (widget-list (find widget elfacturador-item-widgets :test (lambda (w l)
 								       (member w l)))))
      (widget-value-set (elfacturador-item-widget-natura widget-list) natura)
      (widget-value-set (elfacturador-item-widget-iva widget-list) "0"))
    (widget-setup)))

(defun elfacturador-select-all-natura (&rest ignore)
  "Select a particular natura for all the items."
  (save-excursion
    (let* ((natura (elfacturador-completing-read "Seleziona natura articolo: "
						 elfacturador-item-naturas
						 "*ElFacturador Selezione Natura*")))
      (mapc (lambda (item)
	      (widget-value-set (elfacturador-item-widget-natura item) natura)
	      (widget-value-set (elfacturador-item-widget-iva item) "0"))
	    elfacturador-item-widgets)
      (widget-setup))))

(defun elfacturador-iva-widgets-create ()
  "Create only the needed iva subtotal fields."
  (when elfacturador-customer-esIVA-widget
    (when (equal (widget-value elfacturador-customer-esIVA-widget) "esente")
      (elfacturador-select-all-natura)))
  (mapc (lambda (x)
 	  (let ((widgets (cdr x)))
 	    (mapc 'widget-delete widgets)))
 	elfacturador-used-iva-widgets)
  (let ((used-iva (delete-dups (sort
				(mapcar (lambda (item)
					  (string-to-number (widget-value (elfacturador-item-widget-iva item))))
					elfacturador-item-widgets)
				'<))))
    (elfacturador-jump-to-widget (nth 4 elfacturador-button-widgets) 'end)
    (right-char 3)
    (setq elfacturador-used-iva-widgets
	  (mapcar (lambda (iva)
		    (list
		     iva
		     (widget-create 'editable-field
				    :size 5
				    :format "IVA %%: %v  " (format "%5.2f" iva))
		     (widget-create 'editable-field
				    :size 10
				    :format "Imponibile: %v  ")
		     (widget-create 'editable-field
				    :size 10
				    :format "Imposta: %v\n")))
		  used-iva))
    (setq elfacturador-used-iva-widgets
	  (append elfacturador-used-iva-widgets `((nil
						   ,(widget-create 'text
								   :format "\n")))))
    (widget-setup)))

(defun elfacturador-update-sub-total (widgets iva-parameter)
  (let ((tot 0)
	(iva-tot 0))
    (mapc (lambda (x)
	    (let ((qty (string-to-number (widget-value (elfacturador-item-widget-qty x))))
		  (price (string-to-number (widget-value (elfacturador-item-widget-price x))))
		  (iva (string-to-number (widget-value (elfacturador-item-widget-iva x)))))
	      (widget-value-set (elfacturador-item-widget-tot x)
				(format "%9.4f"(* qty price)))
	      (when (= iva iva-parameter)
		(incf tot (* qty price))
		(incf iva-tot (* qty price (/ iva 100.0))))))
	  elfacturador-item-widgets)
    (widget-value-set (second widgets) (format "%10.4f" tot))
    (widget-value-set (third  widgets) (format "%10.4f" iva-tot))))

(defun elfacturador-update-totals (widget &rest ignore)
  "Update total widgets."
  (save-excursion
    (elfacturador-iva-widgets-create)
    (mapc (lambda (x)
	    (elfacturador-update-sub-total (cdr x) (car x)))
	  (butlast elfacturador-used-iva-widgets))
    (let ((tot-notax (apply #'+
			    (mapcar (lambda (x)
				      (string-to-number (widget-value (second (cdr x)))))
				    (butlast elfacturador-used-iva-widgets))))
	  (tot-tax (apply #'+
			  (mapcar (lambda (x)
				    (string-to-number (widget-value (third (cdr x)))))
				  (butlast elfacturador-used-iva-widgets)))))
      (widget-value-set elfacturador-tot-no-tax-widget
			(format "%10.4f" tot-notax))
      (widget-value-set elfacturador-tot-tax-widget
			(format "%10.4f" tot-tax))
      (widget-value-set elfacturador-total-widget (format "%8.2f" (+ tot-notax tot-tax))))))

(defun elfacturador-init-front-end (&rest ignore)
  "Init the fornt-end."
  (switch-to-buffer "*ElFacturador*")
  (kill-all-local-variables)
  (make-local-variable 'elfacturador-item-widgets)
  (text-scale-set 1)
  (setq elfacturador-button-widgets nil)
  (setq elfacturador-item-widgets nil)
  (setq elfacturador-tot-lines 0)
  (setq elfacturador-used-iva-widgets nil)
  (setq elfacturador-doc-month (capitalize (format-time-string "%B")))
  (elfacturador-update-doc-number)
  (let ((inhibit-read-only t))
    (erase-buffer))
  (remove-overlays)
  (widget-insert "El Facturador\n\nFactura como si no hubiera mañana¡¡¡\n\n\n")
  (elfacturador-format-title)
  (push (widget-create 'push-button
		       :notify #'elfacturador-select-doc-type
  		       "Tipo di documento")
	elfacturador-button-widgets)
  (setq elfacturador-doc-type-widget (widget-create 'editable-field
						    :size 10
						    :format ": %v    " "TD01"))
  (setq elfacturador-doc-number-widget (widget-create 'editable-field
						      :size 10
						      :format "Numero: %v    "
						      (concat (number-to-string elfacturador-doc-number) (format-time-string "-%Y"))))
  (setq elfacturador-doc-date-widget (widget-create 'editable-field
						    :size 10
						    :format "Data: %v    "(format-time-string "%Y-%m-%d")))
  (setq elfacturador-doc-money-widget (widget-create 'editable-field
						     :size 10
						     :format "Divisa: %v\n\n\n""EUR"))
  (elfacturador-customer-widgets-create)
  (push (elfacturador-item-widgets-create) elfacturador-item-widgets)
  (push (widget-create 'push-button
		       :notify (lambda (&rest ignore)
				 (beginning-of-line)
				 (push (elfacturador-item-widgets-create) elfacturador-item-widgets)
				 (widget-setup)
				 (widget-backward 11))
		       "Aggiungi linea")
	elfacturador-button-widgets)
  (widget-insert "   ")
  (push (widget-create 'push-button
		       :notify (lambda (&rest ignore)
				 (elfacturador-item-widget-remove))
		       "Elimina linea")
	elfacturador-button-widgets)
  (widget-insert "                                                                     ")
  (push (widget-create 'push-button
		       :notify #'elfacturador-select-all-natura
		       "Natura All.")
	elfacturador-button-widgets)
  (widget-insert "\n\n\n")
  (widget-insert "Totali        ")
  (setq elfacturador-tot-no-tax-widget
	(widget-create 'editable-field
		       :size 10
		       :format "Imponibile: %v  "))
  (setq elfacturador-tot-tax-widget
	(widget-create 'editable-field
		       :size 10
		       :format "Imposta: %v"))
  (setq elfacturador-total-widget
	(widget-create 'editable-field
		       :size 8
		       :format "\n\nTotale fattura: %v"))
  (widget-insert "\n\n")
  (push (widget-create 'push-button
		       :notify #'elfacturador-update-totals
		       "Calcola")
	elfacturador-button-widgets)
  (widget-insert "   ")
  (push (widget-create 'push-button
		       :notify (lambda (&rest ignore)
				 (elfacturador-create-xml)
				 (elfacturador-compile-org-summary)
				 (elfacturador-save-doc-number))
		       "Esporta")
	elfacturador-button-widgets)
  (widget-insert "   ")
  (push (widget-create 'push-button
		       :notify #'elfacturador-init-front-end
		       "Reset")
	elfacturador-button-widgets)
  (widget-insert "   ")
  (push (widget-create 'push-button
		       :notify #'elfacturador-manage-db
		       "Modifica Db")
	elfacturador-button-widgets)
  (widget-insert " ")
  (setq elfacturador-button-widgets (reverse elfacturador-button-widgets))
  (use-local-map widget-keymap)
  (widget-setup)
  (elfacturador-jump-to-widget (nth 1 elfacturador-button-widgets) 'start))

(provide 'elfacturador-front-end)

;;; elfacturador-front-end.el ends here
