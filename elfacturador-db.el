;;; elfacturador-db.el --- elfacturador database

;;; Commentary:

(require 'widget)
(require 'cl)
(require 'elfacturador-items-db)
(require 'elfacturador-customers-db)

;;; Code:

(defconst elfacturador-customers-db "~/elfacturador/elfacturador-customers-db.el"
  "Name of the file where customers data are saved.")
(defconst elfacturador-items-db "~/elfacturador/elfacturador-items-db.el"
  "Name of the file where items data are saved.")

(cl-defstruct elfacturador-item
  name mu price iva nature)

(defun elfacturador-manage-items (&rest ignore)
  "Edit items database."
    (goto-line 2)
  (let ((inhibit-read-only t))
    (delete-region (point) (point-max)))
  (widget-insert "\n")
  (setq elfacturador-active-item (list
				    (widget-create 'push-button
						   :notify #'elfacturador-select-item
						   "Articolo")
				    (widget-create 'editable-field
						   :size 59
						   :format ": %v   ")
 				    (widget-create 'editable-field
						   :size 9
						   :format "Prezzo: %v   ")
				    (widget-create 'editable-field
						   :size 2
						   :format "U.M.: %v   ")
				    (widget-create 'editable-field
						   :size 2
						   :format "IVA: %v")
				    (widget-create 'item
						   :value "\n")))
  (widget-create 'push-button
		 :notify #'elfacturador-save-new-item
		 "Salva")
  (widget-insert "   ")
  (widget-create 'push-button
		       :notify (lambda (&rest ignore)
				 (elfacturador))
		       "Reset")
  (widget-insert "   ")
  (widget-create 'push-button
		 :notify (lambda (&rest ignore)
			   (kill-buffer))
		 "Esci")
  (widget-forward 1)
  (widget-setup))

(defun elfacturador-save-new-item (&rest ignore)
  "Save new item data in file ~/elfacturador/elfacturador-items-db.el."
  (let* ((name (widget-value (nth 1 elfacturador-active-item)))
	(new-entry (cons
		    name
		    (make-elfacturador-item
		     :name name
		     :mu (widget-value (nth 3 elfacturador-active-item))
		     :price (widget-value (nth 2 elfacturador-active-item))
		     :iva (widget-value (nth 4 elfacturador-active-item))))))
    (push new-entry elfacturador-items))
  (with-temp-buffer
    (prin1 `(defconst elfacturador-items ',elfacturador-items) (current-buffer))
    (insert "\n")
    (prin1 '(provide 'elfacturador-items-db) (current-buffer))
    (write-file elfacturador-items-db)
  (message "ARTICOLO SALVATO NEL DATABASE")))

(cl-defstruct elfacturador-person
  name addr civ cap com prov naz pIVA pec sdiCode)

(cl-defstruct (elfacturador-customer (:include elfacturador-person))
  esIVA esIVA-letter)

(cl-defstruct (elfacturador-seller (:include elfacturador-person)))

(defun elfacturador-manage-customers (&rest ignore)
  "Edit customers database."
  (goto-line 2)
  (let ((inhibit-read-only t))
    (delete-region (point) (point-max)))
  (widget-insert "\n")
  (elfacturador-customer-widgets-create)
  (widget-create 'push-button
		 :notify #'elfacturador-save-new-customer
		 "Salva")
  (widget-insert "   ")
  (widget-create 'push-button
		 :notify (lambda (&rest ignore)
			   (let ((inhibit-read-only t))
			     (erase-buffer))
			   (remove-overlays)
			   (elfacturador-manage-customers))
			   "Reset")
  (widget-insert "   ")
  (widget-create 'push-button
		 :notify (lambda (&rest ignore)
			   (kill-buffer))
		 "Esci")
  (widget-forward 1)
  (widget-setup))

(defun elfacturador-save-new-customer (&rest ignore)
  "Save new customer data in file ~/elfacturador/elfacturador-customers-db.el."
  (let ((new-entry (cons
		    (widget-value elfacturador-customer-name-widget)
		    (make-elfacturador-customer
		     :name (widget-value elfacturador-customer-name-widget)
		     :addr (widget-value elfacturador-customer-addr-widget)
		     :civ  (widget-value elfacturador-customer-civ-widget)
		     :cap (widget-value elfacturador-customer-cap-widget)
		     :com (widget-value elfacturador-customer-com-widget)
		     :prov (widget-value elfacturador-customer-prov-widget)
		     :naz (widget-value elfacturador-customer-naz-widget)
		     :pIVA (widget-value elfacturador-customer-pIVA-widget)
		     :pec (widget-value elfacturador-customer-pec-widget)
		     :sdiCode(widget-value elfacturador-customer-sdiCode-widget)
		     :esIVA (widget-value elfacturador-customer-esIVA-widget)
		     :esIVA-letter (widget-value elfacturador-customer-esIVA-letter-widget)))))
    (push new-entry elfacturador-customers))
  (with-temp-buffer
    (prin1 `(defconst elfacturador-customers ',elfacturador-customers) (current-buffer))
    (insert "\n")
    (prin1 '(provide 'elfacturador-customers-db) (current-buffer))
    (write-file elfacturador-customers-db)
  (message "CLIENTE SALVATO NEL DATABASE")))

(defun elfacturador-manage-db (&rest ignore)
  "Open new buffer to edit db."
  (switch-to-buffer "*DbManager*")
  (kill-all-local-variables)
  (let ((inhibit-read-only t))
    (erase-buffer))
  (remove-overlays)
  (text-scale-set 1)
  (widget-insert "El Facturador database manager.\n\n")
  (elfacturador-format-title)
  (widget-create 'push-button
		 :notify #'elfacturador-manage-customers
  		 "Cliente")
  (widget-insert "   ")
  (widget-create 'push-button
		 :notify #'elfacturador-manage-items
  		 "Articolo")
  (use-local-map widget-keymap)
  (widget-backward 2)
  (widget-setup))

(provide 'elfacturador-db)

;;; elfacturador-db.el ends here
