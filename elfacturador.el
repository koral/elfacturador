;;; elfacturador.el --- Interactively compile invoices

;;; Commentary:
;; put files into ~/elfacturador/

;; add to your .emacs:
;; (add-to-list 'load-path "~/elfacturador/")
;; (require 'elfacturador)

;; M-x elfacturador

;; PS install helm to have it prettier

;;; Code:


(require 'widget)
(require 'cl)
(require 'elfacturador-db)
(require 'elfacturador-front-end)

(defun elfacturador ()
  "Facture like ther's no tomorrow."
  (interactive)
  (elfacturador-init-front-end nil))

(provide 'elfacturador)

;;; elfacturador.el ends here
